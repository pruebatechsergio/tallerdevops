var clientesBanderas;

function getClientes() {
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        console.log(request.responseText);
      }
    }
   request.open("GET", url, true);
   request.send();
}

function getClientesBanderas() {

  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        console.log(request.responseText);
        clientesBanderas = request.responseText;
        procesarClientes();
      }
    }
   request.open("GET", url, true);
   request.send();
}

function procesarClientes() {

  var JSONClientes = JSON.parse(clientesBanderas);

  var tabla = document.getElementById("tablaClientes");

  for (var i = 0; i < JSONClientes.value.length; i++) {

    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].CompanyName;

    var columnaPais = document.createElement("td");
    columnaPais.innerText = JSONClientes.value[i].Country;
    var columnaBandera = document.createElement("img");

    var bandera =  JSONClientes.value[i].Country;
    if (JSONClientes.value[i].Country=="UK") {
      var URL = "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + "United-Kingdom" + ".png" ;
    } else {
       var URL = "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + bandera + ".png" ;
    }

    columnaBandera.src = URL;
    columnaBandera.classList.add("flag");
//    columnaBandera.setAttribute('height','50px');
//    columnaBandera.setAttribute('weight','50px');

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPais);
    nuevaFila.appendChild(columnaBandera);

    tabla.appendChild(nuevaFila);
   }
  }
